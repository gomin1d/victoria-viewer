package ua.lokha;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws Exception {
        String file = FileUtils.readFileToString(new File("input.txt"));

        String[] lines = file.split("\n");
        List<Point> points = new LinkedList<>();

        long max = -1;

        for (String line : lines) {
            try {
                if (line.contains("Starting Reading")) {
                    String rangeData = StringUtils.substringBetween(line, "Starting Reading, LBA=", ", sequential access");
                    String[] rangeDatas = rangeData.split("\\.\\.");
                    points.add(new Point(Point.Type.START, Long.parseLong(rangeDatas[0])));
                    max = Math.max(max, Long.parseLong(rangeDatas[1].replace(", FULL", "")));
                } else if (line.contains("Error: UNCR")) {
                    String data = StringUtils.substringBetween(line, "Block ", " Error: UNCR");
                    points.add(new Point(Point.Type.ERR_UNCR, Long.parseLong(data)));
                } else if (line.contains("try REMAP... Error")) {
                    String data = StringUtils.substringBetween(line, "LBA ", " try REMAP");
                    points.add(new Point(Point.Type.ERR_REMAP, Long.parseLong(data)));
                } else if (line.contains("Block start at")) {
                    String data = StringUtils.substringBetween(line, "Block start at ", ") ");
                    points.add(new Point(Point.Type.LONG, Long.parseLong(StringUtils.substringBefore(data, " ("))));
                } /*else {
                points.add(new Point(Point.Type.UNKNOWN, -1));
            }*/
            } catch (Exception e) {
                System.out.println("error line " + line);
                e.printStackTrace();
            }
        }

        points = new LinkedList<>(new HashSet<>(points));
        points.sort(Comparator.comparingLong(point->point.point));

        if (max == -1) {
            max = points.stream()
                    .max(Comparator.comparingLong(point -> point.point))
                    .map(point -> point.point).orElse(0L);
        }

        int height = 5000;
        long it = max / (height - 200);


        BufferedImage image = new BufferedImage(1000, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();

        g.setColor(Color.DARK_GRAY);

        int xLine = 700;

        g.drawLine(xLine, 100, xLine, height - 100);
        g.setColor(Color.GRAY);
        for (int y = 100; y <= height - 100; y += 15) {
            g.drawLine(xLine - 10, y, xLine + 50, y);
            g.drawString(toMB((y - 100) * it) + " (" + toLBA((y - 100) * it) + ")", xLine + 50, y);
        }

        Point last = new Point(Point.Type.UNKNOWN, -100000000000000000L);
        int lastY = 100;
        List<Point> collect = new LinkedList<>();
        g.setColor(Color.RED);
        Iterator<Point> iterator = points.iterator();
        while (iterator.hasNext() || !collect.isEmpty()) {
            Point point = iterator.hasNext() ? iterator.next() : null;
            long minLBA = collect.stream()
                    .mapToLong(p -> p.point)
                    .min().orElse(100);
            int newPoint = toCoord(minLBA, 100, it);

            assert point != null;
            if (!iterator.hasNext() || toCoord(point.point, 100, it) - lastY > 15 || !last.type.equals(point.type)) {
                if (!collect.isEmpty()) {
                    StringBuilder builder = new StringBuilder();
                    builder.append(String.join(", ", collect.stream()
                            .map(p -> p.type.name())
                            .collect(Collectors.toSet())));
                    builder.append(": ");

                    long maxLBA = collect.stream()
                            .mapToLong(p -> p.point)
                            .max().orElse(height - 100);

                    if (collect.size() > 1) {
                        builder.append('[').append(toMB(minLBA)).append("-").append(toMB(maxLBA))
                                .append("] [").append(toLBA(minLBA)).append("-").append(toLBA(maxLBA)).append(']');
                    } else {
                        builder.append(toMB(minLBA))
                                .append(" ").append(toLBA(minLBA));
                    }

                    builder.append(" (count: ").append(collect.size()).append(")");

                    int maxY = Math.max(newPoint, lastY + 15);

                    g.drawLine(xLine, newPoint, xLine - 100, maxY);
                    g.drawString(builder.toString(), xLine - 600, maxY);
                    lastY = maxY;
                }

                collect.clear();
                last = point;
            }

            if (point != null) {
                collect.add(point);
            }
        }

        ImageIO.write(image, "png", new File("output.png"));
    }

    private static int toCoord(long lba, int start, long it) {
        return (int) ((lba / it) + start);
    }

    private static String toMB(long point) {
        long val = (point * 512) / 1000 / 1000;
        return String.format("%,d", val) + "MB";
    }

    private static String toLBA(long lba) {
        return String.format("%,d", lba) + "LBA";
    }

    private static class Point {

        private Type type;
        private long point;

        private Point(Type type, long point) {
            this.type = type;
            this.point = point;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point point1 = (Point) o;

            return point == point1.point && type == point1.type;
        }

        @Override
        public int hashCode() {
            int result = type != null ? type.hashCode() : 0;
            result = 31 * result + (int) (point ^ (point >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return "Point{" +
                    "type=" + type.name() +
                    ", point=" + point +
                    '}';
        }

        private enum Type {
            START,
            ERR_UNCR,
            ERR_REMAP,
            LONG,
            UNKNOWN,

            ;

        }
    }


}
